$(document).ready(function() {
    $("#showPropBtn").attr("disabled", true);
    $("#showNetBtn").attr("disabled", true);
});

$("#hidePropBtn").click(function() {
    $("#submitProposal").attr("hidden", true);
    $("#showPropBtn").removeAttr("disabled");
    $("#hidePropBtn").attr("disabled", true);
});

$("#hideNetBtn").click(function() {
    $("#networkColleagues").attr("hidden", true);
    $("#showNetBtn").removeAttr("disabled");
    $("#hideNetBtn").attr("disabled", true);
});

$("#showPropBtn").click(function() {
    $("#submitProposal").removeAttr("hidden");
    $("#showPropBtn").attr("disabled", true);
    $("#hidePropBtn").removeAttr("disabled");
});

$("#showNetBtn").click(function() {
    $("#networkColleagues").removeAttr("hidden");
    $("#showNetBtn").attr("disabled", true);
    $("#hideNetBtn").removeAttr("disabled");
});