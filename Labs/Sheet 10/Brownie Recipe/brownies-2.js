$("h3").css({ "color": "orange" })

var shopping_list_button = "<button type='button'>View Shopping List</button>";
var ingr_list = $(".ingr");
ingr_list.after(shopping_list_button);

var count = 0;
$("#ingr").on({
    mouseover: function() {
        if (count == 0) {
            var ingredients = $(".ingr li");
            for (var i = 0; i < ingredients.length; i++) {
                var text = ingredients.eq(i).text();
                var checkbox = "<input type='checkbox' name='ingredient'>"
                ingredients.eq(i).prepend(checkbox);
            }
            count++;
        }
    }
});

$("#instr").on({
    mouseover: function() {
        $(".instr").fadeIn(1000);
    },
    mouseout: function() {
        $(".instr").slideUp(1000);
    }
});

$("button").eq(0).click(function() {
    var shopping_list = "<ul id='shoppingList'>";
    var ingredients = $(".ingr li");
    for (var i = 0; i < ingredients.length; i++) {
        var checkbox = $(".ingr li input").eq(i);
        if (checkbox.prop("checked")) {
            var ingredient = "<li>" + ingredients.eq(i).text() + "</li>";
            shopping_list += ingredient;
        }
    }

    shopping_list = shopping_list + "</ul>";
    $("button").eq(0).after(shopping_list);
    $("button").eq(0).attr("disabled", "true");
});