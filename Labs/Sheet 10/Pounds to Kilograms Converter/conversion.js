$("form").eq(0).on("submit", function() {

    if ($("#pValue").val() <= 0) {
        $("#pValue").css({"background-color":"red"});
    } else {
        var pValue = $("#pValue").val();
        var kValue = pValue * 0.45;
        kValue = Math.round(kValue * 100) / 100;
        $("#kValue").text(kValue);
        $("#pValue").css({"background-color":"white"});
    }

    return false;
});

$("form").eq(0).on("reset", function() {
    $(":input[type='number']").val("").css({"background-color":"white"});
    $("#kValue").text("");
});

$("#pValue").on( {
    mouseover: function() {
        // Do nothing
    },
    mouseout: function() {
        $("#kValue").text("");
    }
});