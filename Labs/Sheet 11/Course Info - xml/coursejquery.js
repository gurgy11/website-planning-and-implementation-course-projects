$("#search").click(function() {
    var selectedCourse = $("#course").val();
    var courseInfo = $("#courseInfo");

    $.ajax({
        type: "GET",
        url: "data.xml",
        dataType: "xml",
        success: function(response) {
            $(response).find("course").each(function() {
                var courseNumber = $(this).find("courseNumber").text();
                var courseName = $(this).find("courseName").text();
                var teacher = $(this).find("teacher").text();

                if (selectedCourse == courseNumber) {
                    var infoPara = "<p>";
                    infoPara += courseNumber + " " + courseName + " " + teacher + "</p>";
                    courseInfo.append(infoPara);
                }
            });
        },
        error: function() {
            alert("ERROR!");
        }
    });
});