function displayInfo() {
    var selectCourse = document.getElementById("course").value;
    var courseInfo = document.getElementById("courseInfo");

    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status === 200) {
            var response = xhr.responseXML;
            var courses = response.getElementsByTagName("course");

            for (var i = 0; i < courses.length; i++) {
                var courseNumber = courses[i].getElementsByTagName("courseNumber")[0].firstChild.nodeValue;
                var courseName = courses[i].getElementsByTagName("courseName")[0].firstChild.nodeValue;
                var teacher = courses[i].getElementsByTagName("teacher")[0].firstChild.nodeValue;

                if (courseNumber == selectCourse) {
                    var numberText = document.createTextNode(courseNumber + " ");
                    var nameText = document.createTextNode(courseName + " ");
                    var teacherText = document.createTextNode(teacher);

                    var infoPara = document.createElement("p");
                    infoPara.appendChild(numberText);
                    infoPara.appendChild(nameText);
                    infoPara.appendChild(teacherText);

                    courseInfo.appendChild(infoPara);
                }
            }
        }
    };
    xhr.open("GET", "data.xml", true);
    xhr.send();
}

document.getElementById("search").addEventListener("click", displayInfo, false);