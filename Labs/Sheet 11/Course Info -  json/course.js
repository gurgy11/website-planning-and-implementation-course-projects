function getResponse() {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            var course = response.school[0].course;
            var select = document.getElementById("course").nodeValue;

            for (var i = 0; i < response.school.length; i++) {
                if (select = response.school[i].courseNumber) {
                    var course = response.school[i].course;

                    var courseNumber = course.courseNumber;
                    var numberText = document.createTextNode(courseNumber);
                    var number = document.createElement("p");
                    number.appendChild(numberText);

                    var courseName = course.courseName;
                    var nameText = document.createTextNode(courseName);
                    var name = document.createElement("p");
                    name.appendChild(nameText);

                    var courseTeacher = course.teacher;
                    var teacherText = document.createTextNode(courseTeacher);
                    var teacher = document.createElement("p");
                    teacher.appendChild(teacherText);

                    var courseInfo = document.getElementById("courseInfo");
                    courseInfo.appendChild(number);
                    courseInfo.appendChild(name);
                    courseInfo.appendChild(teacher);
                }
            }
        }
    };
    xhr.open("GET", "data.json", true);
    xhr.send();
}

document.getElementById("search").addEventListener("click", getResponse, false);