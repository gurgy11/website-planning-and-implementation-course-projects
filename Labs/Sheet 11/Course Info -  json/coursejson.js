function displayCourseInfo() {
    var selCourseNumber = document.getElementById("course").value;
    var courseInfo = document.getElementById("courseInfo");

    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status === 200) {
            var r = JSON.parse(xhr.responseText);
            for (var i = 0; i < r.school.length; i++) {
                var course = r.school[i].course;
                var num = course.courseNumber;
                var name = course.courseName;
                var teacher = course.teacher;

                if (selCourseNumber == num) {
                    removeInfo();
                    var infoPara = document.createElement("p");
                    infoPara.innerHTML = num + "<br>" + name + "<br>" + teacher;
                    courseInfo.appendChild(infoPara);

                    var removeBtn = document.createElement("button");
                    removeBtn.innerText = "Remove Info";
                    removeBtn.addEventListener("click", removeInfo, false);
                    courseInfo.appendChild(removeBtn);
                }
            }
        }
    };
    xhr.open("GET", "data.json", true);
    xhr.send();
}

function removeInfo() {
    document.getElementById("courseInfo").innerHTML = "";
}

document.getElementById("search").addEventListener("click", displayCourseInfo, false);