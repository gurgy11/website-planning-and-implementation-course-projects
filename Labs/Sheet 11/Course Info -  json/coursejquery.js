$("#search").click(function() {
    var selCourseNumber = $("#course").val();

    $.ajax({
        type: "GET",
        url: "data.json",
        dataType: "json",
        success: function(response) {
            $.each(response, (function (key, value) {
                $.each(value, (function (key, course) {
                    $.each(course, (function (key, course) {
                        if (course.courseNumber == selCourseNumber) {
                            var infoPara = "<p>" + course.courseNumber + " " + 
                            course.courseName + " " + course.teacher + "</p>";
                            $("#courseInfo").append(infoPara);
                        }
                    }));
                }));
            }));
        },
        error: function() {
            alert("Error!");
        }
    });
});