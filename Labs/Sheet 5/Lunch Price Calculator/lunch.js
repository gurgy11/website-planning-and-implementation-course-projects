function calculatePrice() {
    var inputs = document.getElementsByTagName('input');
    var price = 0.00;

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == 'checkbox') {
            if (inputs[i].checked) {
                price += (+inputs[i].value / 100);
            }
        }
    }

    var tRows = document.getElementsByTagName('td');
    var taxes = (price * 0.15).toFixed(2);

    tRows[1].textContent = price;
    tRows[3].textContent = taxes;
    tRows[5].textContent = "$" + ((+price) + (+taxes)).toFixed(2);
}

function clearAll() {
    var inputs = document.getElementsByTagName('input');

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == 'checkbox') {
            inputs[i].checked = false;
        }
    }

    var tRows = document.getElementsByTagName('td');
    tRows[1].textContent = "0.00";
    tRows[3].textContent = "0.00";
    tRows[5].textContent = "$0.00";
}

document.getElementById('rButton').addEventListener('click', clearAll, false);
document.getElementById('sButton').addEventListener('click', calculatePrice, false);