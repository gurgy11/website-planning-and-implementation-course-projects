$("#ingr").hover(function() {
    $(".ingr > :nth-child(odd)").css("color", "red");
    $(".ingr > :nth-child(even)").css("color", "blue");
    $(".ingr > li").css("font-style", "italic");
},
function() {
    $(".ingr > li").css("color", "black");
    $(".ingr > li").css("font-style", "normal");
});

$("#instr").hover(function() {
    $(".instr").show();
},
function() {
    $(".instr").hide();
});