$("#logo").hide();
$("#logo").css({"width": "500px", "height": "100px"});
$("#windChill").hide();
$("#calculate").css("background-color", "blue");

$("#logo").show(5000);

$("#logo").on({
    mouseover: function(){
        $(this).animate({
            height: "200px",
            width: "1000px"
        }, "slow");
    },
    mouseout: function(){
        $(this).animate({
            height: "100",
            width: "500px"
        }, "slow");
    }
});

$("#calculate").click(function() {
    var temp = $("#temp").val();
    var windSpeed = $("#wind").val();

    if (windSpeed < 0 || temp == "" || windSpeed == "") {
        window.alert("Wind speed must be greater than 0 and all fields must have a value!");
        $("form")[0].reset();
    } else {
        var windChill = temp - (1.5 * windSpeed);

        $("#windChill").text("The wind chill is " + windChill);
        $("#windChill").fadeIn(5000).slideUp(5000);
    }
});

$("#calculate").on({
    mouseover: function(){
        $(this).css("background-color", "green");
    },
    mouseout: function(){
        $(this).css("background-color", "blue");
    }
});

$("#windChill").on({
    mouseover: function(){
        $(this).animate({
            left: "+=200px"
        });
    }
});