var leftImg = document.getElementById('imageLeft');
var rightImg = document.getElementById('imageRight');
var leftImgDefSrc = "imgs/catering1.jpg";
var rightImgDefSrc = "imgs/catering2.jpg";

function wedding() {
    leftImg.setAttribute('src', 'imgs/wedding/wedding1.jpg');
    rightImg.setAttribute('src', 'imgs/wedding/wedding2.jpeg');
}

function halloween() {
    leftImg.setAttribute('src', 'imgs/halloween/halloween1.jpg');
    rightImg.setAttribute('src', 'imgs/halloween/halloween2.jpg');
}

function graduation() {
    leftImg.setAttribute('src', 'imgs/graduation/graduation1.jpg');
    rightImg.setAttribute('src', 'imgs/graduation/graduation2.jpeg');
}

function catering() {
    leftImg.setAttribute('src', leftImgDefSrc);
    rightImg.setAttribute('src', rightImgDefSrc);
}