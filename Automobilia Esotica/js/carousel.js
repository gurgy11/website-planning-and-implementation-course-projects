$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/xml/cars.xml",
        dataType: "xml",
        success: function(xml) {
            var count = 0;
            $(xml).find("car").each(function() {
                var img = $(this).find("image").text();
                $(".carousel-item > img").eq(count).attr("src", img);
                count++;
            });
        },
        error: function() {
            alert("Error loading images into carousel.");
        }
    });
});