$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/xml/cars.xml",
        dataType: "xml",
        success: function(xml) {
            var count = 0;
            $(xml).find("car").each(function() {
                var img = $(this).find("image").text();
                $(".carousel-item > img").eq(count).attr("src", img);
                count++;
            });
        },
        error: function() {
            alert("Error loading images into carousel.");
        }
    });
    $.ajax({
        type: "GET",
        url: "/xml/cars.xml",
        dataType: "xml",
        success: function(xml) {
            var count = 0;
            $(xml).find("car").each(function() {
                if ($(this).find("featured").text() == "true") {
                    var img = $(this).find("image").text();
                    var year = $(this).find("year").text();
                    var make = $(this).find("make").text();
                    var model = $(this).find("model").text();
                    var mileage = $(this).find("mileage").text();
                    var price = $(this).find("price").text();

                    $(".card-img-top").eq(count).attr("src", img);
                    $(".card-title").eq(count).text(year + " " + make + " " + model);
                    $(".year").eq(count).text("Year: " + year);
                    $(".make").eq(count).text("Make: " + make);
                    $(".model").eq(count).text("Model: " + model);
                    $(".mileage").eq(count).text("Mileage: " + mileage);
                    $(".price").eq(count).text("Offering at " + price);

                    count++;
                }
            });
        },
        error: function() {
            alert("Error loading data into cards.");
        }
    });
});